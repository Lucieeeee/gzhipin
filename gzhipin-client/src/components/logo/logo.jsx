import React from 'react'

import logo from './logo-utc.png'
import './logo.less'//专门为这个页面的组件创建，所以直接import


export default function Logo() {
    return (
        <div className="logo-container">
            <img src={logo} alt="logo" className="logo-img"/>
        </div>
    )
}