/*
包含了n个接口请求函数的模块
函数返回值是promise
 */

//也就是说我要向外暴露好一些函数
//暴露函数有两种写法，一种是如下
// export function XXXX() {
//
// }
//我最终是要让ajax帮我发送请求
import ajax from './ajax'
//另一种是用箭头函数 ，这里我们使用注册接口

//注册接口
//箭头后面确定参数
export const reqRegister = (user) => ajax('/register',user, 'POST')
    //想想我们的功能，输入用户名什么的，然后向提交给后台
//登陆接口
export const reqLogin = ({username, password}) => ajax('/login',{username, password}, 'POST')

//更新用户接口
export const reqUpdateUser = (user) => ajax('/update', user, 'POST')
//什么时候用POST，什么时候用GET
//一般查询，要获取查询数据，用GET，POST一般是隐藏提交的数据
// 什么时候用post最多呢，首先你的有一些提交的数据，第二个你这个提交的请求要修改服务器数据库里的数据