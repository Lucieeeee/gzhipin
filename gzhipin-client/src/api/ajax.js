/*
能发送ajax请求的函数模块
函数的返回值是一个promise对象
 */
//如果只要暴露一个的话defalut就够了
//有三个参数 url data数据，是一个对象，里面有username等等的，type是请求方式，这里的默认是GET，因为GET是请求最多的
//最终发请求还是得靠axios
import axios from 'axios'
//axios有两个方法 GET 参数有url 还有一个是POST，参数有url和data，返回promise对象
export default function ajax(url, data={}, type='GET') {
    if(type==='GET'){ //是则发送GET请求
        //我们要拼请求参数的串
        //如果是GET，则要把参数data拼到url里面去
        //就要把字符串拼上去
        //比如说data: {username: Tom, password: 123}
        //最终生成 dataStr: username=Tom&password=123
        //首先遍历string
        let paramStr = ''
        //先建一个空串
        //Object.key(data)得到的是这对象下所有keys的数组，keys是属性名，就是那个username和password组成的数组
        //下面就是要遍历数组来生成串
        //forEach遍历所有的keys，参数是keys，来拿到某一个key
        Object.keys(data).forEach(key => {
            paramStr += key + '=' + data[key] + '&' //但这样对于最后一个会多一个&
        })
        if(paramStr){
            paramStr = paramStr.substring(0,paramStr.length-1)//显示第一个字符到倒数第二个
        }
       //使用axios发送get请求
        return axios.get(url + '?' +paramStr)//直接拼起来
    }else{//不是则发送POST请求
        //使用axios发送post请求
        return axios.post(url,data)
    }
}