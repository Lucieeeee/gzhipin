/*登陆路由组件*/
import  React, {Component} from 'react'
import {NavBar,//导航
    WingBlank,//两翼留白
    List,
    InputItem,
    WhiteSpace,
    Button
} from  'antd-mobile'

import Logo from '../../components/logo/logo'

export default class Login extends Component {
    state = {
        username: '',
        password: '',
    }

    login = () => {
        console.log(this.state)
    }
    //处理输入数据的改变：更新对应的状态
    handleChange = (name,val) =>{
        //更新状态
        this.setState({
            [name]: val //属性名不是name，而是name变量的值
        })
    }

    toRegister = () => {
        this.props.history.replace('/register')
    }
    render(){
        return(
            <div>
                <NavBar>U&nbsp;T&nbsp;C&nbsp;实&nbsp;习</NavBar>
                <WhiteSpace/><WhiteSpace/><WhiteSpace/>
                <Logo/>
                <WhiteSpace/><WhiteSpace/><WhiteSpace/>
                <WingBlank>
                    <List>
                        <InputItem placeholder='请出入用户名' onChange={val => {this.handleChange('username', val)}}>用户名：</InputItem>
                        <WhiteSpace/>
                        <InputItem placeholder='请出入密码' type="password" onChange={val => {this.handleChange('password', val)}}>密&nbsp;&nbsp;&nbsp;码：</InputItem>
                        <WhiteSpace/>
                        <Button type='primary' onClick={this.login}>登陆</Button><WhiteSpace/><WhiteSpace/>
                        <Button type='primary' onClick={this.toRegister}>还没有账户</Button>
                        {/*//用编码的方式实现页面的跳转*/}
                    </List>
                </WingBlank>
            </div>
        )
    }
}