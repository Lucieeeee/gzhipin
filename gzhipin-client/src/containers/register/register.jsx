/*注册理由组件*/
import  React, {Component} from 'react'
import {NavBar,//导航
    WingBlank,//两翼留白
    List,
    InputItem,
    WhiteSpace,
    Radio,//勾选按钮
    Button
    } from  'antd-mobile'

import {connect} from 'react-redux'

//最终我们是要发送一个异步请求，所以要在import
import {register} from '../../redux/actions'

import Logo from '../../components/logo/logo'
const ListItem = List.Item



class Register extends Component {
    state = {
        username: '',
        password: '',
        password2: '',
        type: '',
    }
    //点击注册调用
    register = () => {
        //console.log(this.state)
        //改掉按钮触发事件
        this.props.register(this.state)
    }
    //处理输入数据的改变：更新对应的状态
    handleChange = (name,val) =>{
        //更新状态
        this.setState({
            [name]: val //属性名不是name，而是name变量的值
        })
    }

    toLogin = () => {
        this.props.history.replace('/login')
    }
    render(){
        const {type} = this.state  //如果下面要进行判断的话，就要先把type的值读出来
        const {msg} = this.props.user
        return(
            <div>
                <NavBar>U&nbsp;T&nbsp;C&nbsp;实&nbsp;习</NavBar>
                <WhiteSpace/><WhiteSpace/><WhiteSpace/>
                <Logo/>
                <WhiteSpace/><WhiteSpace/><WhiteSpace/>
                <WingBlank>
                    <List>
                        {/*判断以下msg有没有值*/}
                        {msg?<div className='error-msg'>{msg}</div>: null}
                        <InputItem placeholder='请出入用户名' onChange={val => {this.handleChange('username', val)}}>用户名：</InputItem>
                        <WhiteSpace/>
                        <InputItem placeholder='请出入密码' type="password" onChange={val => {this.handleChange('password', val)}}>密&nbsp;&nbsp;&nbsp;码：</InputItem>
                        <WhiteSpace/>
                        <InputItem placeholder='请出入确认密码' type="password" onChange={val => {this.handleChange('password2', val)}}>确认密码：</InputItem>
                        <WhiteSpace/>
                        <ListItem>
                            <span>用户类型&nbsp;&nbsp;</span>
                            <Radio checked={type==='dashen'} onChange={() => this.handleChange('type','dashen')}>&nbsp;&nbsp;学生&nbsp;&nbsp;</Radio>
                            <Radio checked={type==='laoban'} onChange={() => this.handleChange('type','laoban')}>&nbsp;&nbsp;其他</Radio>
                        </ListItem>
                        <WhiteSpace/><WhiteSpace/>
                        <Button type='primary' onClick={this.register}>注册</Button><WhiteSpace/><WhiteSpace/>
                        <Button type='primary' onClick={this.toLogin}>已有账户</Button>
                        {/*//用编码的方式实现页面的跳转*/}
                    </List>
                </WingBlank>
            </div>
        )
    }
}

//把原来Register的export default 移下来
//要传两个数据,要向UI组件传一个register
//我向Register这个UI组件传了一个
export default connect(
    state => ({user: state.user}), {register})(Register)