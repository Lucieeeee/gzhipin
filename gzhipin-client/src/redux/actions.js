/*
包含n个action creator
异步action
同步action
 */
import {
    AUTH_SUCCESS,
    ERROR_MSG
} from "./action-types";

import {reqRegister,
    reqLogin
} from '../api'

//每一个action-type都对应一个同步action

//授权成功对同步action
//无论是注册成功还是登陆成功，最终要管理的数据都是user
const authSuccess = (user) => ({type: AUTH_SUCCESS, data: user})

//错误提示信息的同步action
const errorMsg = (msg) => ({type: ERROR_MSG, data: msg})





//先写注册异步action
//因为是一个异步action，所以return的是一个函数
//dispatch是一个固定参数
export const register = (user) =>{
    const {username, password, password2, type} = user
    //做表单的前台检查，如果不通过，返回一个errorMsg的同步action
    if(!username){
        return errorMsg('用户名必须指定！')
    }else if(password!==password2){
        return errorMsg('2次密码要一致')
    }
    //说明表单数据合法，返回一个发ajax请求的异步action函数
    return async dispatch => {

        //发送注册的异步ajax请求
        //一旦发送请求，我要去找谁？去找接口请求函数，不去找ajax，因为ajax已经被我包装过了
        //reqRegister 函数需要一个user，但是现在我们函数里面没有user，那我们就需要函数传进来一个user
        //有了promise对象，我们想去取里面的数据，最直接的办法就是then，然后再传回调函数，但是这样写太麻烦
        //我们需要用一个技巧，我们可以用await
        //我们不想要promise，我们想要then，我们想要结果数据
        //以下不是我们想要的结果
        // const promise = reqRegister(user)
        // promise.then(response => {
        //     const result = response.data
        //     //{code: 0/1, msg
        // })
        //我想直接调，然后直接想要结果 但是我们要等待，所以我们要用await
        //但是为什么会报错，因为一旦用上await，前面的dispatch就要变成async
        const response = await reqRegister({username, password, type})
        //这里还涉及到表单数据到前台验证
        //如果验证不通过，表单就要返回一个错误到信息
        const result = response.data
        //下面有两种情况
        //但是不管成功还是失败，我们都要分发同步的action
        //判断完之后是去分发（dispatch）同步action
        //action在action-types中写
        if(result.code===0){ //成功了，就分发授权成功的action
            dispatch(authSuccess(result.data))
        }else {//失败，那就分发失败的action
            dispatch(errorMsg(result.msg))
        }
    }

}

//登陆异步action
export const login = (user) =>{
    const {username, password} = user
    if(!username){
        return errorMsg('用户名必须指定！')
    }else if(!password){
        return errorMsg('密码必须指定')
    }
    return async dispatch => {

        //发送注册的异步ajax请求
        //一旦发送请求，我要去找谁？去找接口请求函数，不去找ajax，因为ajax已经被我包装过了
        //reqRegister 函数需要一个user，但是现在我们函数里面没有user，那我们就需要函数传进来一个user
        //有了promise对象，我们想去取里面的数据，最直接的办法就是then，然后再传回调函数，但是这样写太麻烦
        //我们需要用一个技巧，我们可以用await
        //我们不想要promise，我们想要then，我们想要结果数据
        //以下不是我们想要的结果
        // const promise = reqLogin(user)
        // promise.then(response => {
        //     const result = response.data
        //     //{code: 0/1, msg
        // })
        //我想直接调，然后直接想要结果 但是我们要等待，所以我们要用await
        //但是为什么会报错，因为一旦用上await，前面的dispatch就要变成async
        const response = await reqLogin(user)
        const result = response.data
        //下面有两种情况
        //但是不管成功还是失败，我们都要分发同步的action
        if(result.code===0){ //成功了，就分发授权成功的action
            dispatch(authSuccess(result.data))
        }else {//失败，那就分发失败的action
            dispatch(errorMsg(result.msg))
        }
    }

}