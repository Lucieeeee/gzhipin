/*
包含n个reducer函数：根据老的state和指定的action返回一个新的state
 */
import {combineReducers} from 'redux'

import {
    AUTH_SUCCESS,
    ERROR_MSG
} from './action-types'

const initUser = {username: '',type: '', msg: ''}
//不需要密码，因为后台向前台返回数据的时候是不用返回密码的
//msg用来存错误提示信息

//这里参数state要指定一个初始的对象,产生user状态的reducer
//switch是用来判断执行哪个action的
function user(state=initUser, action) {
    switch (action.type) {
        case AUTH_SUCCESS: //成功的时候data是user
            return {...state, ...action.data}
            //先把原本的状态给解剖出来，接着用action.data给他覆盖掉
            //在reducer中写...是非常多的，因为经常要把一个对象里面的东西弄出来
        case ERROR_MSG:    //失败的时候data是msg
            return {...state, msg:action.data}
        default:
            return state
    }
}




//这个函数是用来管理数据的
export default combineReducers({
   user
})

//最终向外暴露的状态的结构：{user: {}}
//向外暴露user 它是一个对象