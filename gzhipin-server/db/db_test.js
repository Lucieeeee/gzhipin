/*
测试使用mongoose使用mongodb数据库
1） 连接数据库
   1.1 引入mongoose
   1.2 连接指定数据库（URL只有数据库是变化的）
   1.3获取连接对象
   1.4 绑定连接完成的监听（用来提示连接成功）
 */


/*
连接数据库
 */
//这是一个md5加密的函数
// const md5 = require('blueimp-md5')
// // 1.1 引入mongoose
// const mongoose = require('mongoose')
// // 1.2 连接指定数据库（URL只有数据库是变化的）
// mongoose.connect('mongodb://localhost:27017/gzhipin_test', { useNewUrlParser: true })
// // 1.3获取连接对象
// const conn = mongoose.connection
// // 1.4 绑定连接完成的监听（用来提示连接成功）
// conn.on('connected', function () {//连接成功回调
//     console.log('数据库连接成功！！')
// })
// conn.on('error',function () {
//     console.log('数据库连接失败')
// })


/*
2. 得到对应特定集合的Model
 */
//2.1 字义Shema（描述文档结构）文档是对象

// const userSchema = mongoose.Schema({//指定文档的结构 属性名/属性值的类型，是否是必须的，默认值
//     username: {type: String, required: true },  //用户名 类型是string 是否是必须的——是
//     password: {type: String, required: true },
//     type: {type: String, required: true },
//     header: {type: String }//头像不是必要的
// });
// //2.2 定义Model（与集合对应，可以操作集合） 集合是数组
// const UserModel = mongoose.model('user', userSchema) //这个函数第一个参数传什么 根据我们的集合名
// // 集合名：users  这个时候model里面只能是'user' 最后返回的是一个对象


/*
3. 通过Model或其实例对集合数据进行CRUD操作
 */
//3.1 通过Model实例对save（）添加数据

function testSave() {
    //创建UserModel的实例
    //这个函数里面参数传什么呢，最终我们是要创建一个对象，我们要将一个user文档保存一个对象，他是有id的，mongoDB自动生成
    const userModel = new UserModel ({username: 'Tom', password: md5('123'), type: 'laoban'})
  //这里的密码要加密
    //调用save（）保存
    userModel.save (function (err, userDoc) {
        if(err) console.log(err);
        console.log('save()', err, userDoc);
    })
}

//testSave()

//3.2 通过Model的find（）/findOne（）查询多个或一个数据
//查询所有包含匹配文档的数组
function testFind() {
    UserModel.find({_id: '5bf5d26b36481903fa579b75'},function (error, users) {
        console.log('find()',error, users)
    })//这里的UserModel是一个函数对象，用什么方式得到数据，用回调函数
    //查询一个
    UserModel.findOne({_id: '5bf5d26b36481903fa579b75'}, function (error, users) {
        console.log('findOne()',error,users)
    })
}
//testFind()



//3.3 通过Model的findByIdAndUpdate（）更新某个数据
function testUpdate() {
    //先传id，再传更新的数据
    UserModel.findByIdAndUpdate({_id: '5bf5d26b36481903fa579b7c'},{username: 'Jack'}, function(error,users){
        console.log('findByAndUpdate()',error,users)
        })
}
//testUpdate()
//3.4 通过Model的remove（）删除匹配的数据
//这里返回的结果 n是删除的个数，ok是程序运行有没有出错
function testRemove() {
    UserModel.remove({_id: '5bf5d26b36481903fa579b7c'},function (error, doc) {
        console.log('remove()',error,doc)
    })
}

//testRemove()