/*
包含n个操作数据库集合数据的Model模块
1） 连接数据库
   1.1 引入mongoose
   1.2 连接指定数据库（URL只有数据库是变化的）
   1.3获取连接对象
   1.4 绑定连接完成的监听（用来提示连接成功）
 */

const md5 = require('blueimp-md5')
const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/gzhipin', { useNewUrlParser: true })
const conn = mongoose.connection
conn.on('connected', ()=> {
    console.log('db connect success!')
})

/*
2. 得到对应特定集合的Model
 */
//2.1 字义Shema（描述文档结构）文档是对象
//2.2 定义Model（与集合对应，可以操作集合） 集合是数组
//这个函数第一个参数传什么 根据我们的集合名
// 集合名：users  这个时候model里面只能是'user' 最后返回的是一个对象

const userSchema = mongoose.Schema({
    username: {type: String, required: true },  //用户名 类型是string 是否是必须的——是
    password: {type: String, required: true },
    type: {type: String, required: true },
    header: {type: String },//头像不是必要的
    post: {type: String},
    info: {type: String},
    company: {type: String},
    salary: {type: String},
})

//定义Model（与集合对应，可以操作集合）
const UserModel = mongoose.model('user',userSchema)




//2.3 向外暴露Model
exports.UserModel = UserModel
//module.exports = xxx  这个方式只能写一次 叫做一次性暴露
//export.xxx = value 这个方式可以写多次  叫做分别暴露

