var express = require('express');
var router = express.Router();
const md5 = require('blueimp-md5')
const {UserModel} = require('../db/models')//是这个文件里的整体对象
const filter = {password: 0,_v: 0} //查询指定特定的属性






/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

//需要在这里注册一个路由：用来给用户注册 根据需求写代码
/*
a）path为：/register
b）请求方式为：POST
c）接受username和password参数
d）admin是已注册用户
e）注册成功返回：{code: 0, data: {_id: 'abc', username: 'xxx', password:'123}
f）注册失败返回：{code: 1, msg: '此用户已存在'}
*/

//处理请求，返回响应

/*
1.获取请求参数
2.处理
3.返回响应数据
 */
// router.post('/register',function (req, res) {
//   //1.获取请求参数
//     console.log('nihao')
//     const {username, password} = req.body
//     //2.处理
//     if(username==='admin'){ //注册失败
//       //返回响应数据（失败） 返回的是一个对象
//         res.send({code:1, msg: '此用户已存在'})
//     }else { //注册成功
//         res.send({code:0, data: {id: 'abc123', username, password}})
//     }
// })


//注册路由
router.post('/register',function (req, res) {
    //读取请求参数 从req里取数据 因为是post请求，所以是body
    const {username, password, type} = req.body
    //处理,要先查询是不是有这个user
    //根据什么来查询
    UserModel.findOne({username},function (err, user) {
        //如果user有值（已存在）
        if(user) {
            //返回提示错误的信息
            res.send({code: 1, msg: '此用户已存在'})//code为1，说明成功
        } else{ //没值（不存在）
            //保存
            new UserModel({username, password:md5(password), type}).save(function (err, user) {
                //返回包含user的json数据  密码不要返回给前端 所以要先自己封装一个数据
                //登陆有两种技术 一种是cookie还有一种是session？？
                //要向response里面添加一个cookie数据，里面放用户id，交给浏览器保存 cookie数据是键值对数据
                //cookie分两种，一种是绘画cookie，在浏览器的内存里，浏览器关掉就没有了，
                // 一种是持久化cookie，假设实现一天免登录，第三个参数就是来设置有效期的，一旦设置就是持久化cookie
                //现在我们要生成一个cookie(userid: user._id)，并交给浏览器保存
                //用什么生成返回——用res
                //用res中的什么方法——cookie
                //三个参数——1.指定名字 2.值是什么 3.要指定相关信息 如 存活时间
                res.cookie('userid', user._id, {maxAge:1000*60*60*24})
                //这里注册成功就是登陆成功了
                //返回登陆成功信息（包含user）


                const data = {username, type, _id: user._id}//响应数据中不要携带密码
                res.send({code: 0, data})
            })
        }
    })
    //判断用户是否已经存在
    //如果存在返回错误的提示
    //如果不存在，保存
    //1.查询 根据username查询 用什么查 用usermodel查

    //返回响应数据
})

//登陆路由
router.post('/login', function (req, res) {
    const {username, password} = req.body
    //根据username和password查询数据库users，如果没有返回提示错误的信息，如果有，返回登陆成功过的信息（包含user）
    UserModel.findOne({username, password:md5(password)},filter, function (err, user) {
        if(user){ //登陆成功
            res.cookie('userid', user._id, {maxAge:1000*60*60*24})
            //登陆，并且把用户信息暂存一天
            //返回成功的信息
            res.send({code: 0, data:user})
            //这里返回的值应该不包含user的password，具体操作看filter
        }else {//登陆失败
            res.send({code:1, msg:'用户名或密码不正确'})
        }
    })
})

module.exports = router;
