##  Une petite application pour bien apprendre le développment de web

Front-end et back-end sont séparés.

Fonctionnement actuelle: Inscription et connexion

## Les technologies que je choisis:

* Front-end: `React`  `React-Router`  `Redux`  `ES6`  `WebPack`

* Back-end: `Node`  `Express`  `mongodb`

## Plus de détails:

* intéraction entre front/back end : 

request de `AJAX` -> `async/await`

* outil pour tester API: 

`postman`

Afin de tester mon application, vous pourrez entrer des instructions suivants:

```
 npm start (dans <<gzhipin-client>>)
 npm start (dans <<gzhipin-server>>)
 mongod    (dans le terminal)
```








